turnNTRbarplot <- function(treatment, cell.type){
  source("barplotDF.R")
  library("ggplot2")
  #treatment = c("ntr", "bapta", "atpgs1","atpgs10")
  DF <- vector()
  for(t in treatment){
    result <-barplotDF(t,cell.type)
    DF <- rbind(DF, result)
  }
  DF <- subset(DF, DF$turn %in% "middle")
  
  plot= ggplot(DF, aes(x=turn, y=ave, fill=treatment)) +
    geom_bar(position=position_dodge(), colour="black", stat="identity") +
    geom_errorbar(aes(ymin=ave, ymax=ave+sem),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    ylim(0, 50)  +
    labs(y = "NTR intensity") +
    labs(x = NULL) +
    theme(axis.title=element_text(size=20, face="bold"),
          axis.text=element_text(size=20,face="bold"),
          plot.title= element_text(size=24, face="bold"),
          legend.text=element_text(size=14, face="bold"),
          legend.title=element_text(size=14),
          legend.position = "none")

  
  ## ---------- calculating statisitcal significance - wiloxTest-----
  ## -------------------------------------------------------------
  source("statsDF.R")
  DF_stats <- vector()
  for(t in treatment){
    result <- statsDF(t, cell.type)
    DF_stats <- rbind(DF_stats,result)
  }
  s_result <- vector("list")
  for(t in c("apical","middle","basal")){
    data_t <- subset(DF_stats, DF_stats$turn %in% t)
    #if(all(is.na(subset(data_f[[feature]], data_f$Treatment =="control")))| all(is.na(subset(data_c[[feature]], data_c$Treatment =="neo")))){
    #  print(paste(c, "has no comparison groups and skipped"))
    #  next
    #}
    kw <- kruskal.test(data_t$ave ~ data_t$treatment, data=data_t)
    #print(kw)
    posthoc <- pairwise.wilcox.test(data_t$ave, data_t$treatment, p.adj="BH", exact=F)
    posthoc.data<-data.frame(posthoc$p.value)
    significance <- posthoc.data
    significance[significance<0.001]<-"***"
    significance[significance>=0.001 & significance < 0.01] <- "**"
    significance[significance>=0.01 & significance < 0.05] <- "*"
    significance[significance > 0.05] <- "-"
    s_result[[t]] <- significance
  }
  
  print(s_result)
  print(plot)
  
  
}
